export class imageList {
    constructor() {
        this.selectors()
        this.events()
    }

    selectors() {
        this.div = document.querySelector('.div-img');
        this.photo = document.querySelector('.img-imagem');
        this.file = document.querySelector('.item-input-image');
    }

    events() {
        this.div.addEventListener('click', this.clickNaDiv.bind(this));
        this.file.addEventListener('change', this.showPhoto.bind(this));
    }

    clickNaDiv() {
        this.file.click();
    };


    showPhoto() {

        let reader = new FileReader();

        reader.onload = () => {
            this.photo.src = reader.result;
            this.photo.classList.add("active");
        }

        reader.readAsDataURL(this.file.files[0]);

    }
}