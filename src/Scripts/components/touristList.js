export class touristList {
    constructor() {
        this.list = [];
        this.selectors()
        this.events()
    }

    selectors() {
        this.form = document.querySelector('.item-inputs');
        this.items = document.querySelector('.tourist-spots');
        this.imagem = document.querySelector('.img-imagem');
        this.title = document.querySelector('.item-input-title');
        this.description = document.querySelector('.item-input-description');
    }

    events() {
        this.form.addEventListener('submit', this.addItemToList.bind(this));
    }

    resetar() {

        this.imagem.src = '';
        this.title.value = '';
        this.description.value = '';
        this.imagem.classList.remove("active");
    }



    addItemToList(e) {
        e.preventDefault();

        const imageForm = e.target['img-imagem'].src;
        const titleForm = e.target['title-form'].value;
        const descriptionForm = e.target['description-form'].value;



        if (imageForm !== '' && titleForm !== '' && descriptionForm !== '') {

            const item = {
                image: imageForm,
                title: titleForm,
                description: descriptionForm,
            };

            this.list.push(item);
            this.renderListItems();
            this.resetar();


        } else {
            alert('OPA, VOCÊ ESQUECEU QUE PREENCHER ALGO!')
        }
    }

    renderListItems() {
        let itemsStructure = '';

        this.list.forEach((item) => {

            itemsStructure += `
            
                <li class="tourist-spots-results">

                <figure class="tourist-spost-image-content">

                <img class="tourist-spots-image" src="${item.image}"/>
                
                </figure>

                <div class="tourist-spots-content">

                    <h3 class="tourist-spots-title">${item.title}</h3>
                    
                    <p class="tourist-spots-description">${item.description}</p>

                </div>

                
                </li>
            `;

            this.items.innerHTML = itemsStructure;

        });

    }


}